package at.campus02.dbp2.flights;

import at.campus02.dbp2.flights.FlightsDao;
import at.campus02.dbp2.flights.domain.Customer;
import at.campus02.dbp2.flights.domain.Flight;
import at.campus02.dbp2.flights.domain.Seat;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

public class FlightsDaoImpl implements FlightsDao {
    
    private EntityManagerFactory factory;
    private EntityManager manager;
    
    public FlightsDaoImpl(EntityManagerFactory factory) {
        this.factory = factory;
        manager = factory.createEntityManager();
    }
    
    @Override
    public boolean create(Flight flight) {
        
        if (flight == null) {
            return false;
        }
        if (flight.getFlightNumber() == null) {
            return false;
        }
        
        Flight f = manager.find(Flight.class, flight.getFlightNumber());
        if (f != null) {
            return false;
        } else {
            
            for (Seat s : flight.getSeats()) {
                s.setFlight(flight);
            }
            manager.getTransaction().begin();
            manager.persist(flight);
            
            manager.getTransaction().commit();
            Flight newF = manager.find(Flight.class, flight.getFlightNumber());
            
            if (newF == null) {
                return false;
            } else {
                return true;
            }
        }
    }
    
    @Override
    public Flight readFlight(String flightNumber) {
        if (flightNumber == null) {
            return null;
        }
        
        Flight flight = manager.find(Flight.class, flightNumber);
        if (flight == null) {
            return null;
        }
        
        return flight;
    }
    
    @Override
    public Flight update(Flight flight) {
        if (flight == null) {
            return null;
        } else {
            boolean contains = false;
            manager.getTransaction().begin();
            for (Seat s : flight.getSeats())
            {
//                for (Seat s2 : manager.find(Flight.class, flight.getFlightNumber()).getSeats()){
//                    if(s.getId() == s2.getId()){
//                        contains = true;
//                    }
//                }
//                if(contains == false){
    
                    s.setFlight(flight);
//                }
            }
            manager.getTransaction().commit();
            return manager.find(Flight.class, flight.getFlightNumber());
        }
    }
    
    @Override
    public boolean delete(Flight flight) {
        
        if (flight == null) {
            return false;
        }
        
        if (flight.getFlightNumber() == null) {
            return false;
        }
        
        Flight f = manager.find(Flight.class, flight.getFlightNumber());
        if (f == null) {
            return false;
        } else {
            manager.getTransaction().begin();
            manager.remove(flight);
            manager.getTransaction().commit();
            Flight newF = manager.find(Flight.class, flight.getFlightNumber());
            if (newF != null) {
                return false;
            } else {
                return true;
            }
        }
    }
    
    @Override
    public boolean create(Customer customer) {
        
        if (customer == null) {
            return false;
        }
        
        if (customer.getEmail() == null) {
            return false;
        }
        
        Customer c = manager.find(Customer.class, customer.getEmail());
        if (c != null) {
            return false;
        } else {
            manager.getTransaction().begin();
            manager.persist(customer);
            
            manager.getTransaction().commit();
            Customer newC = manager.find(Customer.class, customer.getEmail());
            if (newC == null) {
                return false;
            } else {
                return true;
            }
        }
    }
    
    @Override
    public Customer readCustomer(String email) {
        
        if (email == null) {
            return null;
        }
        
        Customer customer = manager.find(Customer.class, email);
        if (customer == null) {
            return null;
        }
        
        return customer;
    }
    
    @Override
    public Customer update(Customer customer) {
        
        if (customer == null) {
            return null;
        }
        Customer c = manager.find(Customer.class, customer.getEmail());
        if (c == null) {
            return null;
        }
        manager.getTransaction().begin();
        c.setFirstname(customer.getFirstname());
        c.setLastname(customer.getLastname());
        manager.getTransaction().commit();
        
        return manager.find(Customer.class, customer.getEmail());
    }
    
    @Override
    public boolean delete(Customer customer) {
        if (customer == null || customer.getEmail() == null || manager.find(Customer.class, customer.getEmail()) == null) {
            return false;
        } else {
            List<Seat> results = manager.createQuery("SELECT s FROM Seat s WHERE s.customer =:customer", Seat.class).setParameter("customer", customer).getResultList();
            manager.getTransaction().begin();
            for (Seat s : results) {
                s.setCustomer(null);
            }
            manager.remove(customer);
            manager.getTransaction().commit();
            return true;
        }
    }
    
    @Override
    public List<Flight> findFlightsByDestination(String destination) {
        if (destination == null) {
            TypedQuery<Flight> all = manager.createQuery("SELECT f from Flight f ", Flight.class);
            return all.getResultList();
        }
        
        TypedQuery<Flight> query = manager.createQuery("SELECT f from Flight f WHERE lower(f.destination) = :destination", Flight.class);
        return query.setParameter("destination", destination.toLowerCase()).getResultList();
    }
    
    @Override
    public List<Customer> findCustomersBy(String lastname, String firstname) {
        if (firstname == null && lastname != null)
        {
            return manager.createQuery("SELECT c FROM Customer c WHERE lower(c.lastname) =:lastname", Customer.class)
                    .setParameter("lastname", lastname.toLowerCase()).getResultList();
        }
        
        else if (firstname != null && lastname == null)
        {
            return manager.createQuery("SELECT c FROM Customer c WHERE lower(c.firstname) =:firstname", Customer.class)
                    .setParameter( "firstname", firstname.toLowerCase()).getResultList();
        }
        
        else if (firstname == null && lastname == null)
        {
            return manager.createQuery("SELECT c FROM Customer c", Customer.class).getResultList();
        }
        else {
            return manager.createQuery("SELECT c FROM Customer c WHERE lower(c.lastname) =:lastname AND lower(c.firstname) =:firstname", Customer.class)
                    .setParameter("lastname", lastname.toLowerCase()).setParameter("firstname", firstname.toLowerCase()).getResultList();
        }
        
    }
    
    @Override
    public List<Seat> findAvailableSeatsTo(String destination) {
        if (destination == null) {
            return manager.createQuery("SELECT s FROM Seat s WHERE s.customer is null", Seat.class).getResultList();
        }
        return manager.createQuery("SELECT s FROM Seat s WHERE lower(s.flight.destination) =:destination and s.customer is null", Seat.class).setParameter("destination", destination.toLowerCase()).getResultList();
        
    }
    
    @Override
    public List<Seat> findSeatsBookedBy(Customer customer) {
        List<Seat> allSeats = new ArrayList<>();
        try {
            Customer c = manager.find(Customer.class, customer.getEmail());
        } catch (NullPointerException e) {
            return allSeats;
        }
        TypedQuery findSeatsByCustomer = manager.createQuery("SELECT s FROM Seat s WHERE s.customer = :customer", Seat.class);
        allSeats = findSeatsByCustomer.setParameter("customer", customer).getResultList();
        return allSeats;
    }
    
    @Override
    public boolean reserve(Seat seat, Customer customer) {
        
        if (seat == null) {
            return false;
        }
        if (seat.getId() == null) {
            return false;
        }
        if (customer == null) {
            return false;
        }
        if (customer.getEmail() == null) {
            return false;
        }
        if (manager.find(Seat.class, seat.getId()) == null) {
            return false;
        }
        if (manager.find(Customer.class, customer.getEmail()) == null) {
            return false;
        }
        if (seat.getCustomer() != null) {
            return false;
        }
        manager.getTransaction().begin();
        seat.setCustomer(customer);
        manager.getTransaction().commit();
        return true;
    }
    
    @Override
    public boolean cancel(Seat seat, Customer customer) {
        try {
            if (seat.getCustomer().equals(customer)) {
                manager.getTransaction().begin();
                seat.setCustomer(null);
                manager.getTransaction().commit();
                return true;
            }
        } catch (NullPointerException e) {
            return false;
        }
        return false;
    }
    
    @Override
    public void close() {
        if (this.manager != null && this.manager.isOpen()) {
            this.manager.close();
        }
    }
}
