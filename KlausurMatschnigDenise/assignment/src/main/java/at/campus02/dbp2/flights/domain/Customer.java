package at.campus02.dbp2.flights.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Customer
{
   @Id
   private String email;
   private String lastname;
   private String firstname;

   @Override
   public boolean equals(Object o)
   {
      if(this == o)
         return true;
      if(o == null || getClass() != o.getClass())
         return false;

      Customer customer = (Customer) o;
      return email != null ? email.equals(customer.email) : customer.email == null;
   }

   @Override
   public int hashCode()
   {
      return email != null ? email.hashCode() : 0;
   }

   public String getEmail()
   {
      return email;
   }

   public void setEmail(String email)
   {
      this.email = email;
   }

   public String getLastname()
   {
      return lastname;
   }

   public void setLastname(String lastname)
   {
      this.lastname = lastname;
   }

   public String getFirstname()
   {
      return firstname;
   }

   public void setFirstname(String firstname)
   {
      this.firstname = firstname;
   }
}
