package at.campus02.dbp2.flights.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
//@NamedQuery(name="Flight.findFlightsByDestination", query="SELECT f FROM Flight f WHERE")
public class Flight
{
   @Id
   private String flightNumber;
   private String destination;
   @OneToMany (mappedBy = "flight", cascade = CascadeType.ALL, orphanRemoval = true)
   private List<Seat> seats = new ArrayList<>();

   public String getFlightNumber()
   {
      return flightNumber;
   }

   public void setFlightNumber(String flightNumber)
   {
      this.flightNumber = flightNumber;
   }

   public String getDestination()
   {
      return destination;
   }

   public void setDestination(String destination)
   {
      this.destination = destination;
   }

   public List<Seat> getSeats()
   {
      return seats;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      Flight flight = (Flight) o;

      return flightNumber != null ? flightNumber.equals(flight.flightNumber) : flight.flightNumber == null;
   }

   @Override
   public int hashCode() {
      return flightNumber != null ? flightNumber.hashCode() : 0;
   }
}
