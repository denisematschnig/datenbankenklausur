package at.campus02.dbp2.flights.domain;

public enum SeatType {
    AISLE,
    MIDDLE,
    WINDOW
}
