package at.campus02.dbp2.flights.domain;

import javax.persistence.*;

@Entity
//@NamedQuery(name="Seat.findAll", query="SELECT s FROM Seat s")
public class Seat {

    @Id
    @GeneratedValue
    private Integer id;
    private String seatNumber;
    private SeatType seatType;
    @ManyToOne(cascade = CascadeType.PERSIST)
    private Flight flight;
    @ManyToOne (cascade = CascadeType.PERSIST)
    private Customer customer;



    public String getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(String seatNumber) {
        this.seatNumber = seatNumber;
    }

    public SeatType getSeatType() {
        return seatType;
    }

    public void setSeatType(SeatType seatType) {
        this.seatType = seatType;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Integer getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Seat seat = (Seat) o;

        return id != null ? id.equals(seat.id) : seat.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

//    @Override
//    public String toString() {
//        return "Seat [id=" + id + ", seatnumber=" + seatNumber + ", seatType=" + seatType + ", flight=" + flight + ", customer="
//                + customer + "]";
//    }

}
