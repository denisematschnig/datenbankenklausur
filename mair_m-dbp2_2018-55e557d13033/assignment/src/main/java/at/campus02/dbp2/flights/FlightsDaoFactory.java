package at.campus02.dbp2.flights;

import at.campus02.dbp2.flights.domain.FlightsDaoImpl;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class FlightsDaoFactory {

    private EntityManagerFactory factory = Persistence.createEntityManagerFactory("assignment");
    private FlightsDaoImpl flightsDao = new FlightsDaoImpl(factory);

    public FlightsDaoFactory(EntityManagerFactory factory) {

    }

    public FlightsDao getFlightsDao() {
        return flightsDao;
    }
}
