package at.campus02.dbp2.flights.domain;

import at.campus02.dbp2.flights.FlightsDao;

import javax.persistence.*;
import java.util.*;

public class FlightsDaoImpl implements FlightsDao
{

   private EntityManagerFactory factory;
   private EntityManager manager;

   public FlightsDaoImpl(EntityManagerFactory factory)
   {
      this.factory = factory;
      manager = factory.createEntityManager();
   }

   @Override
   public boolean create(Flight flight)
   {
      if(flight == null) {
         return false;
      } else if(manager.find(Flight.class, flight.getFlightNumber()) == null) {

         for(Seat s : flight.getSeats()) {
            s.setFlight(flight);
         }

         manager.getTransaction().begin();
         manager.persist(flight);
         manager.getTransaction().commit();
         if(manager.contains(manager.find(Flight.class, flight.getFlightNumber()))) {
            return true;
         }
      }
      return false;
   }

   @Override
   public Flight readFlight(String flightNumber)
   {
      if(flightNumber == null) {
         return null;
      }
      return manager.find(Flight.class, flightNumber);
   }

   @Override
   public Flight update(Flight flight)
   {

      return null;
   }

   @Override
   public boolean delete(Flight flight)
   {
      if(flight == null || flight.getFlightNumber() == null) {
         return false;
      } else {
         Flight f = manager.find(Flight.class, flight.getFlightNumber());
         if(f == null) {
            return false;
         } else {

            manager.getTransaction().begin();
            manager.remove(f);
            manager.getTransaction().commit();
            return true;
         }
      }
   }

   @Override
   public boolean create(Customer customer)
   {

      if(customer == null) {
         return false;
      }
      if(customer.getEmail() == null) {
         return false;
      }

      if(readCustomer(customer.getEmail()) != null) {
         return false;
      }

      manager.getTransaction().begin();
      manager.persist(customer);
      manager.getTransaction().commit();
      return true;

   }

   @Override
   public Customer readCustomer(String email)
   {
      if(email == null) {
         return null;
      }

      Customer c = manager.find(Customer.class, email);
      return c;
   }

   @Override
   public Customer update(Customer customer)
   {
      try {
         Customer c = manager.find(Customer.class, customer.getEmail());
         manager.getTransaction().begin();
         c.setFirstname(customer.getFirstname());
         c.setLastname(customer.getLastname());
         manager.getTransaction().commit();
         return manager.find(Customer.class, customer.getEmail());
      } catch(NullPointerException e) {
         return null;
      }

   }

   @Override
   public boolean delete(Customer customer)
   {
      if(customer.getEmail() == null) {
         return false;
      }
      Customer c = manager.find(Customer.class, customer.getEmail());
      if(c == null) {
         return false;
      }
      manager.getTransaction().begin();
      manager.remove(c);
      manager.getTransaction().commit();
      return true;
   }

   @Override
   public List<Flight> findFlightsByDestination(String destination)
   {
      return null;
   }

   @Override
   public List<Customer> findCustomersBy(String lastname, String firstname)
   {
      return null;
   }

   @Override
   public List<Seat> findAvailableSeatsTo(String destination)
   {
      return null;
   }

   @Override
   public List<Seat> findSeatsBookedBy(Customer customer)
   {
      return null;
   }

   @Override
   public boolean reserve(Seat seat, Customer customer)
   {
      return false;
   }

   @Override
   public boolean cancel(Seat seat, Customer customer)
   {
      return false;
   }

   @Override
   public void close()
   {
      if(manager == null) {
         manager.close();
      }
   }
}
