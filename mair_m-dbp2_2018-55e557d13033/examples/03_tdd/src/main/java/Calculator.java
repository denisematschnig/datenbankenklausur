import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {


  public static final String DELIMITER = "[,\n]";
  public static final Pattern PATTERN = Pattern.compile("//(.)\n(.*)");

  public int add(String numbers) {
    if (numbers.isEmpty()) {
      return 0;
    }

    String delimiter = determineDelimiter(numbers);
    String calculationString = determineCalculationString(numbers);

    String[] numberStrings = calculationString.split(delimiter);

    StringBuilder errors = new StringBuilder();
    int sum = 0;
    for (String numberString : numberStrings) {
      sum += parse(numberString, errors);
    }

    ensureNoErrors(errors);

    return sum;
  }

  private void ensureNoErrors(StringBuilder errors) {
    if (errors.length() > 0) {
      throw new IllegalArgumentException("negatives not allowed: " +
              errors.toString());
    }
  }

  private int parse(String numberString, StringBuilder errors) {
    int number = Integer.parseInt(numberString);
    if (number < 0) {
      errors.append(number);
    }
    return number;
  }

  private String determineCalculationString(String numbers) {
    Matcher matcher = PATTERN.matcher(numbers);
    if (matcher.find()) {
      return matcher.group(2);
    }
    return numbers;
  }

  private String determineDelimiter(String numbers) {
    Matcher matcher = PATTERN.matcher(numbers);
    if (matcher.find()) {
      return matcher.group(1);
    }
    return DELIMITER;
  }


}
