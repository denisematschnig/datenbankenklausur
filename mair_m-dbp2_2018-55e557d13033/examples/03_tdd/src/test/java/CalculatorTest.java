import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class CalculatorTest {

    @Test
    public void emptyStringResultsInZero() {
        // setup:
        Calculator calculator = new Calculator();

        // when:
        int result = calculator.add("");

        // then:
        assertThat(result, is(0));
    }

    @Test
    public void singleNumberResultsInThatNumber() {
        // setup:
        Calculator calculator = new Calculator();

        // when:
        int result = calculator.add("1");

        // then:
        assertThat(result, is(1));

        // when / then
        assertThat(calculator.add("5"), is(5));
        assertThat(calculator.add("17"), is(17));
    }

    @Test
    public void addsTwoCommaSeparatedNumbers() {
        // setup:
        Calculator calculator = new Calculator();

        // when / then:
        assertThat(calculator.add("1,2"), is(3));
        assertThat(calculator.add("17,4"), is(17 + 4));
    }

    @Test
    public void addsMultipleNumbers() {
        // setup:
        Calculator calculator = new Calculator();

        // when / then:
        assertThat(calculator.add("1,2,3"), is(6));
        assertThat(calculator.add("17,4,34,41"), is(17 + 4 + 34 + 41));
    }

    @Test
    public void allowsNewlineAsDelimiter() {
        // setup:
        Calculator calculator = new Calculator();

        // when / then
        assertThat(calculator.add("1\n2"), is(3));
        assertThat(calculator.add("17\n4,34\n41"), is(17 + 4 + 34 + 41));
    }

    @Test
    public void allowsCustomDelimiters() {
        // setup:
        Calculator calculator = new Calculator();

        // when / then:
        assertThat(calculator.add("//a\n1a2"), is(3));
        assertThat(calculator.add("//;\n17;4;34;41"), is(17 + 4 + 34 + 41));
    }

    @Test(expected = RuntimeException.class)
    public void negativeNumbersThrowException() {
        // setup:
        Calculator calculator = new Calculator();

        // when
        calculator.add("1,-1,2");
    }

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void negativeNumberExceptionContainsExpectedMessage() {
        // setup:
        Calculator calculator = new Calculator();

        // when / then:
        exception.expect(RuntimeException.class);
        exception.expectMessage("negatives not allowed");
        calculator.add("1,-1,2");
    }

    @Test
    public void negativeNumberExceptionContainsWrongNumbersInMessage() {
        // setup:
        Calculator calculator = new Calculator();

        // when / then:
        exception.expect(RuntimeException.class);
        exception.expectMessage("-1");
        exception.expectMessage("-4");
        calculator.add("1,-1,2,-4");
    }
}
