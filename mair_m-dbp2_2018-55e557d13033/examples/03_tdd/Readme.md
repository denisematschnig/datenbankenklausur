## Beispiel zu TDD (test driven development)

Als Einführung/Auffrischung zu "test driven development" soll ein bekanntes [_Kata_](https://en.wikipedia.org/wiki/Kata_(programming)) durchgeführt werden - der ["String Calculator"](http://osherove.com/tdd-kata-1/).

### Hinweise

* Halten Sie sich an die Anweisungen in der Aufgabenstellung (siehe Link oben). Arbeiten Sie nicht voraus, sondern immer nur am nächsten Test. 
* Als Vorgehensweise sollte ["red-green-refactor"](https://www.jamesshore.com/Blog/Red-Green-Refactor.html) angewandt werden. Also zuerst einen Testfall erstellen, der sollte fehlschlagen (_red_). Dann den entsprechenden Code dazu anpassen. Wenn der Test dann erfolgreich durchläuft (_green_), sollte der Code bei weiterhin erfolgreichen Tests verbessert werden (_refactor_).
* Beim Erstellen können wir in IntelliJ ein gradle-Projekt verwenden. Dabei werden die Source-Folder für Programm und Tests bereits eingerichtet sowie die ``junit``-Library zum Testen eingebunden. 

### Aufgabenstellung

siehe [http://osherove.com/tdd-kata-1/](http://osherove.com/tdd-kata-1/)
