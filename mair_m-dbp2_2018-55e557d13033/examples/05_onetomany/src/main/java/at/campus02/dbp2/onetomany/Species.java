package at.campus02.dbp2.onetomany;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Species {

    @Id @GeneratedValue
    private Integer id;
    private String name;

    @OneToMany (mappedBy = "species",
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<Animal> animals = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public List<Animal> getAnimals() {
        return animals;
    }

    @Override public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Species species = (Species) o;

        return id != null ? id.equals(species.id) : species.id == null;
    }

    @Override public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
