package at.campus02.dbp2.onetomany;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Animal {

    @Id @GeneratedValue
    private Integer id;
    private String name;

    @ManyToOne (cascade = CascadeType.PERSIST)
    private Species species;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    @Override public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Animal animal = (Animal) o;

        return id != null ? id.equals(animal.id) : animal.id == null;
    }

    @Override public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
