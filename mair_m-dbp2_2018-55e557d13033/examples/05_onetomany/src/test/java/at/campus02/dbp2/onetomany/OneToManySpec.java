package at.campus02.dbp2.onetomany;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class OneToManySpec {

    private EntityManagerFactory factory;
    private EntityManager manager;

    @Before
    public void before() {
        factory = Persistence.createEntityManagerFactory(
                "persistenceUnitName");
        manager = factory.createEntityManager();
    }

    @After
    public void after() {
        if (manager != null && manager.isOpen()) {
            manager.close();
        }
        if (factory != null && factory.isOpen()) {
            factory.close();
        }
    }

    @Test
    public void persistSpeciesAlsoPersistsAnimals() {
        // given
        Animal rabbit = new Animal();
        rabbit.setName("rabbit");
        Animal cat = new Animal();
        cat.setName("cat");

        Species mammals = new Species();
        mammals.setName("mammals");

        mammals.getAnimals().add(rabbit);
        mammals.getAnimals().add(cat);
        // falls refresh nicht möglich: selber Referenzen verwalten
        // vor dem ersten persist kein refresh möglich
        // -> daher hier jedenfalls selbst verwalten
        rabbit.setSpecies(mammals);
        cat.setSpecies(mammals);

        // when
        manager.getTransaction().begin();
        manager.persist(mammals);
        manager.getTransaction().commit();
        manager.clear();

        // then
        Animal rabbitFromDb = manager.find(Animal.class, rabbit.getId());
        Animal catFromDb = manager.find(Animal.class, cat.getId());
        Species mammalsFromDb = manager.find(Species.class, mammals.getId());

        assertThat(mammalsFromDb.getAnimals().size(), is(2));
        assertThat(mammalsFromDb.getAnimals().contains(rabbitFromDb), is(true));
        assertThat(mammalsFromDb.getAnimals().contains(catFromDb), is(true));

        assertThat(rabbitFromDb.getSpecies(), is(mammalsFromDb));
    }

    @Test
    public void testUpdate() {
        // given
        Animal clownfish = new Animal();
        clownfish.setName("clownfish");
        Animal shark = new Animal();
        shark.setName("shark");
        Animal squirrel = new Animal();
        squirrel.setName("squirrel");

        Species fish = new Species();
        fish.setName("fish");

        clownfish.setSpecies(fish);
        fish.getAnimals().add(clownfish);

        squirrel.setSpecies(fish);
        fish.getAnimals().add(squirrel);

        manager.getTransaction().begin();
        manager.persist(fish);
        manager.getTransaction().commit();

        // when
        manager.getTransaction().begin();
        // weg mit dem Eichhörnchen
        fish.getAnimals().remove(squirrel);
        manager.remove(squirrel);
        // dafür kommt der Hai dazu
        shark.setSpecies(fish);
        fish.getAnimals().add(shark);
        manager.persist(shark);
        manager.getTransaction().commit();

        manager.clear();

        // then
        Species fishFromDb = manager.find(Species.class, fish.getId());
        Animal squirrelFromDb = manager.find(Animal.class, squirrel.getId());
        Animal clownfishFromDb = manager.find(Animal.class, clownfish.getId());
        Animal sharkFromDb = manager.find(Animal.class, shark.getId());

        // squirrel soll nicht mehr existieren
        assertThat(squirrelFromDb, is(nullValue()));

        // fish soll clownfish und shark beinhalten
        assertThat(fishFromDb.getAnimals().size(), is(2));
        assertThat(fishFromDb.getAnimals(), hasItems(sharkFromDb, clownfishFromDb));

        // shark und clownfish sind Fische
        assertThat(clownfishFromDb.getSpecies(), is(fish));
        assertThat(sharkFromDb.getSpecies(), is(fish));
    }


    @Test
    public void testUpdateWithOrphanRemoval() {
        // given
        Animal clownfish = new Animal();
        clownfish.setName("clownfish");
        Animal squirrel = new Animal();
        squirrel.setName("squirrel");

        Species fish = new Species();
        fish.setName("fish");

        clownfish.setSpecies(fish);
        fish.getAnimals().add(clownfish);

        squirrel.setSpecies(fish);
        fish.getAnimals().add(squirrel);

        manager.getTransaction().begin();
        manager.persist(fish);
        manager.getTransaction().commit();

        // when
        manager.getTransaction().begin();
        // weg mit dem Eichhörnchen
        fish.getAnimals().remove(squirrel);
// bei orphanRemoval brauchen wir das nicht selbst löschen
//        manager.remove(squirrel);
        manager.getTransaction().commit();

        manager.clear();

        // then
        Species fishFromDb = manager.find(Species.class, fish.getId());
        Animal squirrelFromDb = manager.find(Animal.class, squirrel.getId());
        Animal clownfishFromDb = manager.find(Animal.class, clownfish.getId());

        // squirrel soll nicht mehr existieren
        assertThat(squirrelFromDb, is(nullValue()));

        // fish soll clownfish beinhalten
        assertThat(fishFromDb.getAnimals().size(), is(1));
        assertThat(fishFromDb.getAnimals(), hasItems(clownfishFromDb));

        // clownfish ist ein Fisch
        assertThat(clownfishFromDb.getSpecies(), is(fish));
    }











}
