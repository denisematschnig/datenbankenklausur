## jpa Mappings und Queries Beispiel

### Verwendete Libraries

* ``lib/derby.jar``: Simple "embedded" Datenbank, Implementierung in java, inklusive jdbc Treiber
* ``lib/javax.persistence_....jar``: Java Persistence API, nur die Schnittstelle, keine tatsächliche Implementierung.
* ``lib/eclipselink.jar``: JPA Implementierung von EclipseLink

### Aufgabenstellung

Erstellen einer einfachen java-Anwendung zum Speichern bzw. Auslesen von "Student"-Objekten in/von einer dateibasierten Datenbank (derby) mittels jpa.
Wir mappen verschiedene Datentypen und eine automatisch vergebene "technische" ID. 

Außerdem sollen ein paar Queries mittels JPQL implementiert werden. 

* Erzeugen der java-Anwendung (``Application.java`` inklusive ``main``-Methode)
* Anlegen der ``Student`` Klasse mit folgenden Properties:
    - ``id``: Integer, PrimaryKey in der Datenbank
    - ``lastname``: String
    - ``firstname``: String
    - ``birthday``: java.time.LocalDate
    - ``gender``: Verwendung eines entsprechenden Enums

  Achtung Angabenänderung: Für ``birthday`` wird nicht mehr ``java.util.Date`` verwendet!
  
* Konfigurationsdatei ``persistence.xml`` anlegen und konfigurieren.
* Anlegen des ``StudentDao`` Interface mit folgenden Methoden:
    - ``Student create(Student student)``
    - ``Student update(Student student)``
    - ``void delete(Student student)``
    * ``Student find(Integer id)``
    * ``List<Student> findAll()``
    * ``List<Student> findAllByLastname(String lastname)``
    * ``List<Student> findAllBornBefore(int year)``
* Implementieren des Interface als ``StudentDaoImpl``.
* Implementieren ``Application``:
    * ``EntityManagerFactory`` erzeugen lassen (über ``Persistence``) für die konfigurierte Persistence-Unit
    * Verwendung des ``StudentDao`` für alle Datenbank-Operationen (mit ``StudentDaoImpl`` als Implementierung).
    * Erzeugen und Persistieren einiger Student-Instanzen
    * Ändern einer Student-Instanz
    * Löschen einer Student-Instanz
    * Ausführen sämtlicher Query-Methoden (und Anzeige der Resultate)
    * Aufräumen der Ressourcen
* Falls noch zu viel Zeit übrig ist, kann das ``StudentDao`` Interface um eine Methode ``List<Student> findAllByGender(Gender gender)`` erweitert werden, für welche ``StudentDaoImpl`` eine in der ``Student`` Klasse definierte _NamedQuery_ verwendet (``@NamedQuery`` Annotation)
