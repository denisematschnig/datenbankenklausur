package at.campus02;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.time.LocalDate;
import java.time.Month;
import java.util.List;

public class StudentDaoImpl implements StudentDao {

    private EntityManagerFactory factory;
    private EntityManager manager;

    public StudentDaoImpl(EntityManagerFactory factory) {
        this.factory = factory;
        manager = factory.createEntityManager();
    }

    @Override public Student create(Student student) {
        manager.getTransaction().begin();
        manager.persist(student);
        manager.getTransaction().commit();
        return student;
    }

    @Override public Student update(Student student) {

        // wenn der student nicht "managed" ist, muss
        // man ihn trotzdem mergen.
        // manager.detach(student);

        manager.getTransaction().begin();
        //  -> siehe Kommentar darüber - hier müsste man mergen
        // manager.merge(student);
        manager.getTransaction().commit();
        return student;
    }

    @Override public void delete(Student student) {
        manager.getTransaction().begin();
        manager.remove(student);
        manager.getTransaction().commit();
    }

    @Override public Student find(Integer id) {
        return manager.find(Student.class, id);
    }

    @Override public List<Student> findAll() {
        return manager.createQuery("select s from Student s", Student.class)
                .getResultList();
    }

    @Override public List<Student> findAllByLastname(String lastname) {
        return manager.createQuery(
                "select s from Student s where s.lastname = :lastname",
                Student.class)
                .setParameter("lastname", lastname)
                .getResultList();
    }

    @Override public List<Student> findAllBornBefore(int year) {
        return manager.createQuery(
                "select s from Student s where s.birthday < :firstDayOfYear",
                Student.class)
                .setParameter("firstDayOfYear",
                        LocalDate.of(year, Month.JANUARY, 1))
                .getResultList();
    }

    @Override public List<Student> findAllByGender(Gender gender) {
        return manager.createNamedQuery("Student.findAllByGender", Student.class)
                .setParameter("gender", gender)
                .getResultList();
    }

    @Override public void close() {
        if (manager != null && manager.isOpen()) {
            manager.close();
        }
    }
}
