package at.campus02;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Application {

    private static EntityManagerFactory factory =
            Persistence.createEntityManagerFactory("persistenceUnitName");

    public static void main(String[] args) {

        StudentDao studentDao = new StudentDaoImpl(factory);

        System.out.println("-- CREATE --------------------------------------");
        Student student1 = prepareStudent("Hansi",
                "Hinterseer", "02.02.1954", Gender.MALE);
        System.out.println(studentDao.create(student1));
        Student student2 = prepareStudent("Friederike",
                "Falballa", "04.03.1977", Gender.FEMALE);
        System.out.println(studentDao.create(student2));
        Student student3 = prepareStudent("Georg",
                "Hirsch", "11.07.1997", Gender.MALE);
        System.out.println(studentDao.create(student3));
        Student student4 = prepareStudent("Helene",
                "Hirsch", "10.09.1985", Gender.FEMALE);
        System.out.println(studentDao.create(student4));

        System.out.println("-- UPDATE --------------------------------------");
        student2.setLastname("Forstheimer");
        System.out.println(studentDao.update(student2));

        System.out.println("-- FIND ----------------------------------------");
        System.out.println(studentDao.find(student1.getId()));

        System.out.println("-- DELETE --------------------------------------");
        studentDao.delete(student1);
        System.out.println(" Deleted: " + student1.getId() + " - " +
                        studentDao.find(student1.getId()));


        System.out.println("-- QUERIES -------------------------------------");
        System.out.println(studentDao.findAll());
        System.out.println(studentDao.findAllByLastname("Hirsch"));
        System.out.println(studentDao.findAllBornBefore(1990));
        System.out.println(studentDao.findAllByGender(Gender.FEMALE));

        // cleanup
        studentDao.close();
        if (factory.isOpen()) {
            factory.close();
        }

    }

    private static Student prepareStudent(String firstname,
            String lastname, String birthdayString, Gender gender) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate birthday = LocalDate.parse(birthdayString, formatter);
        return new Student(firstname, lastname, birthday, gender);
    }
}
