package at.campus02.dbp2.onetoone;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class OtherBidirectional {

    @Id @GeneratedValue
    private Integer id;

    @OneToOne ( mappedBy = "other")
    private OneBidirectional one;

    public Integer getId() {
        return id;
    }

    public OneBidirectional getOne() {
        return one;
    }

    public void setOne(OneBidirectional one) {
        this.one = one;
    }
}
