package at.campus02.dbp2.onetoone;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class OneBidirectional {

    @Id @GeneratedValue
    private Integer id;

    @OneToOne ( cascade = CascadeType.ALL)
    private OtherBidirectional other;

    public Integer getId() {
        return id;
    }

    public OtherBidirectional getOther() {
        return other;
    }

    public void setOther(OtherBidirectional other) {
        this.other = other;
    }
}
