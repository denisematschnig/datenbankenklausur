package at.campus02.dbp2.onetoone;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Other {

    @Id @GeneratedValue
    private Long id;

    public Long getId() {
        return id;
    }
}
