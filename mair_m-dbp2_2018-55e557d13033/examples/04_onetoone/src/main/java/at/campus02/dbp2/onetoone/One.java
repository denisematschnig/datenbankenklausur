package at.campus02.dbp2.onetoone;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class One {

    @Id @GeneratedValue
    private Long id;

    @OneToOne
    private Other other;

    public Long getId() {
        return id;
    }

    public Other getOther() {
        return other;
    }

    public void setOther(Other other) {
        this.other = other;
    }
}
