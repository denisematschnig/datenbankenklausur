package at.campus02.dbp2.onetoone;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

public class OneToOneSpec {

    private EntityManagerFactory factory;
    private EntityManager manager;

    @Before
    public void before() {
        factory = Persistence.createEntityManagerFactory(
                "persistenceUnitName");
        manager = factory.createEntityManager();
    }

    @After
    public void after() {
        if (manager != null && manager.isOpen()) {
            manager.close();
        }
        if (factory != null && factory.isOpen()) {
            factory.close();
        }
    }

    @Test
    public void persistWithoutTransactionDoesNotStoreInDb() {
        // given
        One one = new One();
        Other other = new Other();
        one.setOther(other);

        // when
        manager.persist(one);
        // Cache vom EntityManager leeren, damit die Objekte
        // neu aus der DB geholt werden müssen.
        manager.clear();

        // then
        One fromDb = manager.find(One.class, one.getId());
        assertThat(fromDb, is(nullValue()));
    }

    @Test
    public void persistStoresRelationIfBothEntitiesArePersisted() {
        // given
        One one = new One();
        Other other = new Other();
        one.setOther(other);

        // when
        manager.getTransaction().begin();
        manager.persist(one);
        manager.persist(other);
        manager.getTransaction().commit();
        // Cache vom EntityManager leeren, damit die Objekte
        // neu aus der DB geholt werden müssen.
        manager.clear();

        // then
        One oneFromDb = manager.find(One.class, one.getId());
        Other otherFromDb = manager.find(Other.class, other.getId());
        assertThat(oneFromDb.getOther(), is(otherFromDb));

    }

    @Test
    public void persistWithCascadeStoresOtherInDatabase() {
        // given
        OneWithCascade one = new OneWithCascade();
        Other other = new Other();
        one.setOther(other);

        // when
        manager.getTransaction().begin();
        manager.persist(one);
// bei Cascade (PERSIST oder ALL) braucht man verbundene Objekte nicht
// mehr selber persistieren.
//        manager.persist(other);
        manager.getTransaction().commit();
        // Cache vom EntityManager leeren, damit die Objekte
        // neu aus der DB geholt werden müssen.
        manager.clear();

        // then
        OneWithCascade oneFromDb = manager.find(OneWithCascade.class, one.getId());
        Other otherFromDb = manager.find(Other.class, other.getId());
        assertThat(oneFromDb.getOther(), is(otherFromDb));
    }

    @Test
    public void bidirectionalClosesBothSidesOfRelation() {
        // given
        OneBidirectional one = new OneBidirectional();
        OtherBidirectional other = new OtherBidirectional();
        // Referenzen selbst im Speicher verwalten! (oder neu einlesen aus DB).
        one.setOther(other);
        other.setOne(one);

        // when
        manager.getTransaction().begin();
        manager.persist(one);
        manager.getTransaction().commit();
        manager.clear();

        // then
        OneBidirectional oneFromDb = manager.find(OneBidirectional.class, one.getId());
        OtherBidirectional otherFromDb = manager.find(OtherBidirectional.class, other.getId());

        assertThat(oneFromDb.getOther(), is(otherFromDb));
        assertThat(otherFromDb.getOne(), is(oneFromDb));

    }


    @Test
    public void bidirectionalRelationNeedsRefreshIfNotBothSidesSetManually() {
        // given
        OneBidirectional one = new OneBidirectional();
        OtherBidirectional other = new OtherBidirectional();
        // In diesem Test verwalten wir nur eine Seite der Relation
        // -> dafür müssen wir dann aber refresh verwenden!
        one.setOther(other);

        // when
        manager.getTransaction().begin();
        manager.persist(one);
        manager.getTransaction().commit();

        // then
        OneBidirectional oneFromDb = manager.find(OneBidirectional.class, one.getId());
        OtherBidirectional otherFromDb = manager.find(OtherBidirectional.class, other.getId());

        // also: refresh
        manager.refresh(otherFromDb);

        assertThat(oneFromDb.getOther(), is(otherFromDb));
        assertThat(otherFromDb.getOne(), is(oneFromDb));
    }























}
