package at.campus02;

import java.util.List;

public interface StudentDao {

  void create(Student student);
  Student read(Integer rollNumber);
  void update(Student student);
  void delete(Student student);

  List<Student> getAllStudents();

}
