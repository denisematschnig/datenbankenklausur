package at.campus02;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StudentDaoDbImpl implements StudentDao {

  private String connectionUrl;
  private Connection connection;

  public StudentDaoDbImpl(String connectionUrl) {
    this.connectionUrl = connectionUrl;
    initConnection();
    if (!ensureStudentsTable()) {
      throw new IllegalStateException("Could not create students table.");
    }
  }

  private boolean ensureStudentsTable() {
    try {
      PreparedStatement statement = connection.prepareStatement(
              "create table STUDENTS "
                      + "(rollnumber int primary key, "
                      + "lastname varchar(30), "
                      + "firstname varchar(30))");
      statement.execute();
    } catch (SQLException e) {
      throw new IllegalStateException(e);
    }
    return true;
  }

  private void initConnection() {
    String driver = "org.apache.derby.jdbc.EmbeddedDriver";
    try {
      Class.forName(driver).newInstance();
      connection = DriverManager.getConnection(connectionUrl);
    } catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) {
      throw new IllegalStateException(e);
    }
  }

  @Override
  public void create(Student student) {
    PreparedStatement insert = null;
    try {
      insert = connection.prepareStatement(
              "insert into STUDENTS values (?,?,?)");
      insert.setInt(1, student.getRollNumber());
      insert.setString(2, student.getLastname());
      insert.setString(3, student.getFirstname());
      insert.executeUpdate();
    } catch (SQLException e) {
      throw new IllegalStateException(e);
    }
  }

  @Override
  public Student read(Integer rollNumber) {
    Student student = null;
    try {
      PreparedStatement query = connection.prepareStatement(
              "select * from STUDENTS where ROLLNUMBER = ?");
      query.setInt(1, rollNumber);
      ResultSet rs = query.executeQuery();
      if (rs.next()) {
        student = new Student();
        student.setRollNumber(rs.getInt(1));
        student.setLastname(rs.getString(2));
        student.setFirstname(rs.getString(3));
      }
    } catch (SQLException e) {
      throw new IllegalStateException(e);
    }
    return student;
  }

  @Override
  public void update(Student student) {
    try {
      PreparedStatement update = connection.prepareStatement(
              "update STUDENTS set lastname=?, firstname=?"
                      + " where ROLLNUMBER=?");
      update.setString(1, student.getLastname());
      update.setString(2, student.getFirstname());
      update.setInt(3, student.getRollNumber());
      update.executeUpdate();
    } catch (SQLException e) {
      throw new IllegalStateException(e);
    }
  }

  @Override
  public void delete(Student student) {
    try {
      PreparedStatement delete = connection.prepareStatement(
              "delete from STUDENTS where ROLLNUMBER=?");
      delete.setInt(1, student.getRollNumber());
      delete.execute();
    } catch (SQLException e) {
      throw new IllegalStateException(e);
    }
  }

  @Override
  public List<Student> getAllStudents() {
    List<Student> result = new ArrayList<>();
    try {
      PreparedStatement query =  connection.prepareStatement(
              "select * from STUDENTS");
      ResultSet rs = query.executeQuery();
      while (rs.next()) {
        Student student = new Student();
        student.setRollNumber(rs.getInt(1));
        student.setLastname(rs.getString(2));
        student.setFirstname(rs.getString(3));
        result.add(student);
      }
    } catch (SQLException e) {
      throw new IllegalStateException(e);
    }
    return result;
  }
}
