package at.campus02;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;

public class StudentDaoJpaImpl implements StudentDao {

  private EntityManagerFactory factory;

  public StudentDaoJpaImpl(EntityManagerFactory factory) {
    this.factory = factory;
  }

  @Override
  public void create(Student student) {
    EntityManager manager = factory.createEntityManager();
    try {
      manager.getTransaction().begin();
      manager.persist(student);
      manager.getTransaction().commit();
    } finally {
      manager.close();
    }
  }

  @Override
  public Student read(Integer rollNumber) {
    EntityManager manager = factory.createEntityManager();
    try {
      return manager.find(Student.class, rollNumber);
    } finally {
      manager.close();
    }
  }

  @Override
  public void update(Student student) {
    EntityManager manager = factory.createEntityManager();
    try {
      manager.getTransaction().begin();
      if (!manager.contains(student)) {
        manager.merge(student);
      }
      manager.getTransaction().commit();
    } finally {
      manager.close();
    }
  }

  @Override
  public void delete(Student student) {
    EntityManager manager = factory.createEntityManager();
    try {
      manager.getTransaction().begin();
      Student merged = manager.merge(student);
      manager.remove(merged);
      manager.getTransaction().commit();
    } finally {
      manager.close();
    }
  }

  @Override
  public List<Student> getAllStudents() {
    EntityManager manager = factory.createEntityManager();
    try {
      TypedQuery<Student> query = manager.createQuery(
              "select s from Student s",
              Student.class
      );
      return query.getResultList();
      // alternativ mit Builder-Pattern:
      //  return manager.createQuery("select s from Student s", Student.class)
      //                .getResultList();
    } finally {
      manager.close();
    }
  }
}
