package at.campus02;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Student {

  @Id
  private Integer rollNumber;
  private String firstname;
  private String lastname;


  public Integer getRollNumber() {
    return rollNumber;
  }

  public void setRollNumber(Integer rollNumber) {
    this.rollNumber = rollNumber;
  }

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("Student{");
    sb.append("rollNumber=").append(rollNumber);
    sb.append(", firstname='").append(firstname).append('\'');
    sb.append(", lastname='").append(lastname).append('\'');
    sb.append('}');
    return sb.toString();
  }


}
