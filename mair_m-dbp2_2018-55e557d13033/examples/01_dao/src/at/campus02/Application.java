package at.campus02;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Application {

  private static EntityManagerFactory factory =
          Persistence.createEntityManagerFactory(
                  "nameOfPersistenceUnitInPersistenceXml");

  public static void main(String[] args) {

    // Variante 1: im Speicher
//    StudentDao dao = new StudentDaoMemoryImpl();
    // Variante 2: JDBC, handgestrickt
//    StudentDao dao =
//            new StudentDaoDbImpl(
//                    "jdbc:derby:database;create=true");
    // Variante 3: JPA
    StudentDao dao = new StudentDaoJpaImpl(factory);




    // CREATE
    Student first = new Student();
    first.setRollNumber(100);
    first.setLastname("Schistal");
    first.setFirstname("Kevin");
    dao.create(first);

    Student second = new Student();
    second.setRollNumber(200);
    second.setLastname("Pistulka");
    second.setFirstname("Pauli");
    dao.create(second);

    // READ ALL
    System.out.println("READ ALL: " + dao.getAllStudents());

    // READ
    Student firstFromStorage = dao.read(100);
    System.out.println("READ: " + firstFromStorage);

    // UPDATE
    Student updated = new Student();
    updated.setRollNumber(first.getRollNumber());
    updated.setLastname(first.getLastname());
    updated.setFirstname("Franz");
    dao.update(updated);
    System.out.println("UPDATE: " + dao.read(updated.getRollNumber()));


    // DELETE
    dao.delete(second);
    System.out.println("DELETE: " + dao.getAllStudents());
  }

}
