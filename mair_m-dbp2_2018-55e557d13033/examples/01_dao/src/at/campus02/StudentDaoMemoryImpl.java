package at.campus02;

import java.util.ArrayList;
import java.util.List;

public class StudentDaoMemoryImpl implements StudentDao {

  private List<Student> students = new ArrayList<>();

  @Override
  public void create(Student student) {
    students.add(student);
  }

  @Override
  public Student read(Integer rollNumber) {
    if (rollNumber == null) {
      return null;
    }
    for (Student student : students) {
      if (rollNumber.equals(student.getRollNumber())) {
        return student;
      }
    }
    return null;
  }

  @Override
  public void update(Student student) {
    if (student == null) {
      return;
    }
    Student stored = read(student.getRollNumber());
    if (stored != null) {
      // Variante 1: direkt am Objekt ändern
      // -> Referenzen außerhalb (in main) werden auch geändert!
      //    (first außen ändert sich)
      //stored.setFirstname(student.getFirstname());
      //stored.setLastname(student.getLastname());

      // Variante 2: direkt in der Liste ersetzen
      students.remove(stored);
      students.add(student);
    }
  }

  @Override
  public void delete(Student student) {
    if (student == null) {
      return;
    }
    // warum nochmals aus der Liste lesen?
    // -> damit das remove verlässlich funktioniert.
    //    Ist kein equals auf dem Student definiert, wird ein
    //    Referenz-Vergleich gemacht (identisches Objekt im Speicher)
    //    Dadurch könnte remove nicht funktionieren, wenn zwar
    //    die Information in "student" dieselbe ist, genau dieses
    //    Objekt aber nicht in der Liste gespeichert ist.
    Student stored = read(student.getRollNumber());
    if (stored != null) {
      students.remove(stored);
    }
  }

  @Override
  public List<Student> getAllStudents() {
    return students;
  }
}
